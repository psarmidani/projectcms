<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AppController extends Controller
{
    /**
     * @Route("/", name="app")
     */
    public function index()
    {
        $users = new UserController();

        echo '<pre>';
        print_r($users->cgetAction());
        exit;

        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }
}
